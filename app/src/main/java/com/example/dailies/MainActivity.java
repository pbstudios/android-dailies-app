package com.example.dailies;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
{
    private Menu menu;
    private RecyclerView dailiesRecyclerView;
    private LinearLayout noDailiesView;

    private DailyViewModel dailyViewModel;
    private DailyListAdapter dailyListAdapter;

    public static final int CREATE_DAILY = 10;
    private String LOG = "------> MainActivity";

    private boolean allDailiesChecked = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);

        final Intent newDailyIntent = new Intent(this, DailyActivity.class);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivityForResult(newDailyIntent, CREATE_DAILY);

                /*Snackbar.make(view, "Add Daily", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
            }
        });

        dailiesRecyclerView = findViewById(R.id.recyclerView);
        noDailiesView = findViewById(R.id.no_dailies_view);

        // setup daily ListAdapter
        dailyListAdapter = new DailyListAdapter(this);
        dailiesRecyclerView.setAdapter(dailyListAdapter);
        dailiesRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // setup daily ViewModel
        dailyViewModel = ViewModelProviders.of(this).get(DailyViewModel.class);
        dailyViewModel.getAllDailies().observe(this, new Observer<List<Daily>>() {
            @Override
            public void onChanged(@Nullable final List<Daily> dailies)
            {
                // Update the cached copy of the words in the adapter.
                dailyListAdapter.setDailies(dailies);
                if(dailyListAdapter.getItemCount() > 0)
                {
                    dailiesRecyclerView.setVisibility(View.VISIBLE);
                    noDailiesView.setVisibility(View.GONE);
                }
                else
                {
                    dailiesRecyclerView.setVisibility(View.GONE);
                    noDailiesView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.d(LOG, "onResume() called");
        if(getIntent()!= null && getIntent().getExtras()!= null)
        {
            Intent justGotAnUpdateIntent = getIntent();

            Daily toUpdate = (Daily) justGotAnUpdateIntent.getExtras().getSerializable("DAILY");
            boolean isInEditMode = justGotAnUpdateIntent.getExtras().getBoolean(DailyActivity.EDIT_TAG);

            Log.d(LOG, "RECEIVED isInEditMode as: " + isInEditMode);

            if(isInEditMode)
            {
                Log.d(LOG, "updating existing view model\n" + toUpdate.toString());
                dailyViewModel.update(toUpdate);
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        if(id == android.R.id.home)
        {
            if(getTitle().equals("Delete dailies"))
            {
                setTitle("Dailies");

                menu.findItem(R.id.delete_dailies).setVisible(false);
                menu.findItem(R.id.select_all_dailies).setVisible(false);

                List<Daily> currentDailies = dailyListAdapter.getDailies();
                for(int i = 0; i < currentDailies.size(); i++)
                {
                    currentDailies.get(i).setCheckBoxVisible(false);
                    currentDailies.get(i).setCheckBoxChecked(false);
                }
                dailyListAdapter.setDailies(currentDailies);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                return true;
            }
        }
        else if (id == R.id.action_settings)
        {
            Intent settingsIntent = new Intent(this, SettingsActivity.class);
            startActivity(settingsIntent);
            return true;
        }
        else if(id == R.id.delete_dailies) // with trashcan icon
        {
            List<Daily> dailiesToRemove = new ArrayList<>();
            List<Daily> currentDailies = dailyListAdapter.getDailies();
            for(int i = 0; i < currentDailies.size(); i++)
            {
                if (currentDailies.get(i).isCheckBoxVisible()
                    && currentDailies.get(i).isCheckBoxChecked())
                {
                    dailiesToRemove.add(currentDailies.get(i));
                    //dailyViewModel.deleteDaily(currentDailies.get(i));
                }
            }
            if(dailiesToRemove.size() > 0)
            {
                for(int i = 0; i < dailiesToRemove.size(); i++)
                {
                    dailyViewModel.deleteDaily(dailiesToRemove.get(i));
                }
                // hide the trash can
                menu.findItem(R.id.delete_dailies).setVisible(false);
                menu.findItem(R.id.select_all_dailies).setVisible(false);

                setTitle("Dailies");

                getSupportActionBar().setDisplayHomeAsUpEnabled(false);

                if(dailyListAdapter.getDailies().size() == 0)
                {
                    dailiesRecyclerView.setVisibility(View.GONE);
                    noDailiesView.setVisibility(View.VISIBLE);
                }
            }
            else
            {
                Utility.Toast(this, "Please select a daily to delete");
            }
        }
        else if(id == R.id.select_all_dailies)
        {
            List<Daily> currentDailies = dailyListAdapter.getDailies();

            for(int i = 0; i < currentDailies.size(); i++)
            {
                currentDailies.get(i).setCheckBoxVisible(true);
                currentDailies.get(i).setCheckBoxChecked(allDailiesChecked);
            }
            dailyListAdapter.setDailies(currentDailies);
            allDailiesChecked = !allDailiesChecked;
        }
        return super.onOptionsItemSelected(item);
    }
    public void deleteADaily()
    {
        List<Daily> dailies = dailyListAdapter.getDailies();
        for(int i = 0; i< dailies.size(); i++)
        {
            dailies.get(i).setCheckBoxVisible(true);
        }
        dailyListAdapter.setDailies(dailies);
        setTitle("Delete dailies");

        menu.findItem(R.id.delete_dailies).setVisible(true);
        menu.findItem(R.id.select_all_dailies).setVisible(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(LOG, "onActivityResult -> \nrequestcode: " + requestCode + "\nresultcode: "  + resultCode);

        // you want a result code of -1
        if(requestCode == CREATE_DAILY && resultCode == RESULT_OK)
        {
            Daily daily = (Daily) data.getExtras().getSerializable("DAILY");
            Log.d(LOG, "New Daily to insert is\n" + daily.toString());
            if(daily != null)
            {
                dailiesRecyclerView.setVisibility(View.VISIBLE);
                noDailiesView.setVisibility(View.GONE);

                Log.d(LOG, "inserting new daily into the view model");
                dailyViewModel.insert(daily);
            }
            else
            {
                Log.d(LOG, "onActivityResult - daily was null");
            }
        }
        else
        {
            Log.d(LOG, "onActivityResult - result was not ok");
        }
    }
}
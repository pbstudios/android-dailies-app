package com.example.dailies;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class DailyActivity extends AppCompatActivity
{
    private String LOG = "------> DailyActivity";

    public static String EDIT_TAG = "isInEditMode";
    //private DailyTimeViewModel dailyTimeViewModel;
    public boolean isInEditMode = false;

    private int numOfDaysSelected = 0;
    private int numOfTimesCreated = 0;

    private int selectedColorWhiteID;
    private int unselectedColorBlackID;

    private Drawable selected;
    private Drawable unselected;

    private EditText titleEditText, descEditText;

    public static RecyclerView timesRecyclerView;
    public static TextView noTimesSetTextview;

    private DailyTimeListAdapter dailyTimesListAdapter;

    private List<Button> Day_Buttons = new ArrayList<>();
    private List<DailyTime> dailyTimes = new ArrayList<>();

    private Daily editableDaily;

    private Menu menu;

    private boolean allTimesChecked = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // layout setup
        setContentView(R.layout.activity_add_dailly);

        titleEditText = findViewById(R.id.title_edittext);
        descEditText = findViewById(R.id.desc_edittext);

        timesRecyclerView = findViewById(R.id.times_list);
        noTimesSetTextview = findViewById(R.id.no_reminder_times);

        // setup timesList adapter
        dailyTimesListAdapter = new DailyTimeListAdapter(this);
        dailyTimesListAdapter.setDailyTimes(dailyTimes); // this is an empty list at this point

        timesRecyclerView.setAdapter(dailyTimesListAdapter);
        timesRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // REMOVED VIEW MODEL STUFF FROM dailyTimes
        // setup dailyTime ViewModel
        /*dailyTimeViewModel = ViewModelProviders.of(this).get(DailyTimeViewModel.class);
        dailyTimeViewModel.getAllDailyTimes().observe(this, new Observer<List<DailyTime>>()
        {
            @Override
            public void onChanged(@Nullable final List<DailyTime> dailyTimes)
            {
                adapter.setDailyTimes(dailyTimes);
            }
        });*/

        Button monButton, tueButton, wedButton, thursButton, friButton, satButton, sunButton;

        monButton = findViewById(R.id.monButton);
        tueButton = findViewById(R.id.tueButton);
        wedButton = findViewById(R.id.wedButton);
        thursButton = findViewById(R.id.thurButton);
        friButton = findViewById(R.id.friButton);
        satButton = findViewById(R.id.satButton);
        sunButton = findViewById(R.id.sunButton);

        Day_Buttons.add(monButton);
        Day_Buttons.add(tueButton);
        Day_Buttons.add(wedButton);
        Day_Buttons.add(thursButton);
        Day_Buttons.add(friButton);
        Day_Buttons.add(satButton);
        Day_Buttons.add(sunButton);

        selected = getResources().getDrawable(R.drawable.weekday_selected);
        unselected = getResources().getDrawable(R.drawable.weekday_unselected);
        selectedColorWhiteID = getResources().getColor(R.color.colorWhite);
        unselectedColorBlackID = getResources().getColor(R.color.colorBlack);

        // make sure all buttons are unselected (clean slate)
        for(int i = 0; i < Day_Buttons.size();i++)
        {
            Day_Buttons.get(i).setBackground(unselected);
        }

        // EDITING ZONE
        if(getIntent() != null && getIntent().getExtras() != null)
        {
            isInEditMode = getIntent().getExtras().getBoolean(EDIT_TAG);
            Log.d(LOG, "onCreate - EDITING ZONE - isInEditMode: " + isInEditMode);

            setTitle(R.string.title_edit_daily);
            editableDaily = (Daily) getIntent().getExtras().getSerializable("DAILY");

            titleEditText.setText(editableDaily.title);
            descEditText.setText(editableDaily.description);

            Log.d(LOG, "onCreate - editableDaily.getReminderAtTimes().size is: "+ editableDaily.getRemindAtTimes().size());
            if(editableDaily.getRemindAtTimes().size() > 0)
            {
                timesRecyclerView.setVisibility(View.VISIBLE);
                noTimesSetTextview.setVisibility(View.GONE);

                dailyTimes = editableDaily.getRemindAtTimes();
                dailyTimesListAdapter.setDailyTimes(dailyTimes);

                numOfTimesCreated = editableDaily.getRemindAtTimes().size();
            }
            List<Integer> days = editableDaily.getRemindOnDays();
            for (int i = 0; i < days.size(); i++)
            {
                //numOfDaysSelected++;
                Utility.Day day = Utility.convertFromInt(days.get(i));
                switch (day)
                {
                    case Monday:
                        onButtonPressed(monButton);
                        break;
                    case Tuesday:
                        onButtonPressed(tueButton);
                        break;
                    case Wednesday:
                        onButtonPressed(wedButton);
                        break;
                    case Thursday:
                        onButtonPressed(thursButton);
                        break;
                    case Friday:
                        onButtonPressed(friButton);
                        break;
                    case Saturday:
                        onButtonPressed(satButton);
                        break;
                    case Sunday:
                        onButtonPressed(sunButton);
                        break;
                    default:
                        break;
                }
            }
        }
    }
    public void onButtonPressed(View view)
    {
        Button button = (Button) view;

        // is currently selected -> make it unselected
        if(button.getBackground() == selected)
        {
            button.setBackground(unselected);
            button.setTextColor(unselectedColorBlackID);
            numOfDaysSelected--;
        }
        // is currently not selected -> make it selected
        else if(button.getBackground() == unselected)
        {
            button.setBackground(selected);
            button.setTextColor(selectedColorWhiteID);
            numOfDaysSelected++;
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.menu_add_daily, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == android.R.id.home)
        {
            if(getTitle().equals("Delete times"))
            {
                if(isInEditMode)
                {
                    setTitle("Edit daily");
                }
                else
                {
                    setTitle("Add daily");
                }

                List<DailyTime> currentDailyTimes = dailyTimesListAdapter.getDailyTimes();
                for(int i = 0; i < currentDailyTimes.size(); i++)
                {
                    currentDailyTimes.get(i).setCheckBoxVisible(false);
                    currentDailyTimes.get(i).setCheckBoxChecked(false);
                }
                dailyTimesListAdapter.setDailyTimes(currentDailyTimes);

                menu.findItem(R.id.add_daily).setVisible(true);
                menu.findItem(R.id.select_all_times).setVisible(false);
                menu.findItem(R.id.delete_times).setVisible(false);
                return true;
            }
        }
        else if (id == R.id.add_daily)
        {
            List<Integer> daysSelected = new ArrayList<>();
            boolean canCreateDaily = false;
            String errorMessage = "Complete all fields";

            if (titleEditText.getText().toString().length() > 0)
            {
                for (int i = 0; i < Day_Buttons.size(); i++)
                {
                    if (Day_Buttons.get(i).getBackground() == selected)
                    {
                        daysSelected.add(i);
                    }
                }
                if (numOfDaysSelected > 0)
                {
                    if (numOfTimesCreated > 0)
                    {
                        canCreateDaily = true;
                        for(int i = 0; i < dailyTimes.size(); i++)
                        {
                            dailyTimes.get(i).setCheckBoxVisible(false);
                        }
                        dailyTimesListAdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        errorMessage += "\nSelect a time";
                    }
                }
                else
                {
                    errorMessage += "\nSelect a Day";
                }
            }
            else
            {
                errorMessage += "\nAdd a title";
            }

            if (canCreateDaily)
            {
                Daily dailyToSendAccross = null;

                if (isInEditMode)
                {
                    editableDaily.setTitle(titleEditText.getText().toString());
                    editableDaily.setDescription(descEditText.getText().toString());
                    editableDaily.setRemindOnDays(daysSelected);
                    Log.d(LOG, "onDone pressed, editable daily has time count: " + dailyTimes.size());
                    editableDaily.setRemindAtTimes(dailyTimes);
                    dailyToSendAccross = editableDaily;
                }
                else
                {
                    dailyToSendAccross = new Daily
                            (
                                titleEditText.getText().toString(),
                                descEditText.getText().toString(),
                                daysSelected,
                                dailyTimes
                            );
                }
                Intent returnIntent = new Intent(this, MainActivity.class);

                Bundle bundle = new Bundle();
                bundle.putSerializable("DAILY", dailyToSendAccross);
                bundle.putBoolean(EDIT_TAG, isInEditMode);

                returnIntent.putExtras(bundle);

                if (!isInEditMode)
                {
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
                else
                {
                    startActivity(returnIntent);
                    finish();
                }
                return true;
            }
            else
            {
                Utility.Toast(this, errorMessage);
            }
        }
        else if (id == R.id.clear_daily)
        {
            clearDaily();
        }
        else if (id == R.id.reset_days)
        {
            resetDays();
        }
        else if(id == R.id.select_all_times)
        {
            List<DailyTime> currentDailyTimes = dailyTimesListAdapter.getDailyTimes();

            for(int i = 0; i < currentDailyTimes.size(); i++)
            {
                Log.d(LOG, "daily time @ " + i);

                currentDailyTimes.get(i).setCheckBoxVisible(true);
                currentDailyTimes.get(i).setCheckBoxChecked(allTimesChecked);
            }
            dailyTimesListAdapter.setDailyTimes(currentDailyTimes);
            allTimesChecked = !allTimesChecked;
        }
        /*else if (id == R.id.delete_a_time)
        {
            for(int i = 0; i < dailyTimes.size(); i++)
            {
                dailyTimes.get(i).setCheckBoxVisible(true);
            }
            dailyTimesListAdapter.notifyDataSetChanged();
            setTitle("Delete times");

            MenuItem doneMenuItem = menu.findItem(R.id.add_daily);
            doneMenuItem.setVisible(false);

            MenuItem deleteTimesMenuItem = menu.findItem(R.id.delete_times);
            deleteTimesMenuItem.setVisible(true);
        }*/
        else if(id == R.id.delete_times)
        {
            List<DailyTime> dailyTimesToRemove = new ArrayList<>();
            List<DailyTime> currentDailyTimes = dailyTimesListAdapter.getDailyTimes();

            for(int i = 0; i < currentDailyTimes.size(); i++)
            {
                if (currentDailyTimes.get(i).isCheckBoxChecked()
                    &&
                        currentDailyTimes.get(i).isCheckBoxVisible())
                {
                    dailyTimesToRemove.add(currentDailyTimes.get(i));
                }
            }
            if(dailyTimesToRemove.size() > 0)
            {
                for(int i = 0; i < dailyTimesToRemove.size(); i++)
                {
                    currentDailyTimes.remove(dailyTimesToRemove.get(i));
                    numOfTimesCreated--;
                }
                dailyTimesListAdapter.setDailyTimes(currentDailyTimes);

                for(int i = 0; i < currentDailyTimes.size(); i++)
                {
                    currentDailyTimes.get(i).setCheckBoxChecked(false);
                    currentDailyTimes.get(i).setCheckBoxVisible(false);
                }
                dailyTimesListAdapter.setDailyTimes(currentDailyTimes);

                if(isInEditMode)
                {
                    setTitle("Edit daily");
                }
                else
                {
                    setTitle("Add daily");
                }

                menu.findItem(R.id.select_all_times).setVisible(false);
                menu.findItem(R.id.delete_times).setVisible(false);
                menu.findItem(R.id.add_daily).setVisible(true);

                if(currentDailyTimes.size() == 0)
                {
                    //graphics
                    noTimesSetTextview.setVisibility(View.VISIBLE);
                    timesRecyclerView.setVisibility(View.GONE);
                }
            }
            else
            {
                Utility.Toast(this, "Please select times to delete");
            }
        }
        return super.onOptionsItemSelected(item);
    }
    public void deleteTime()
    {
        for(int i = 0; i < dailyTimes.size(); i++)
        {
            dailyTimes.get(i).setCheckBoxVisible(true);
        }
        dailyTimesListAdapter.notifyDataSetChanged();

        setTitle("Delete times");

        menu.findItem(R.id.add_daily).setVisible(false);
        menu.findItem(R.id.delete_times).setVisible(true);
        menu.findItem(R.id.select_all_times).setVisible(true);
    }
    public void clearDaily()
    {
        titleEditText.setText("");
        descEditText.setText("");

        resetDays();
        resetTimes();
    }
    public void resetDays()
    {
        // data
        numOfDaysSelected = 0;

        // graphics
        for(int i = 0; i < Day_Buttons.size();i++)
        {
            Day_Buttons.get(i).setBackground(unselected);
            Day_Buttons.get(i).setTextColor(unselectedColorBlackID);
        }
    }
    public void resetTimes()
    {
        // data
        numOfTimesCreated = 0;
        dailyTimes.clear();
        dailyTimesListAdapter.setDailyTimes(dailyTimes);

        //dailyTimeViewModel.deleteAll();

        //graphics
        noTimesSetTextview.setVisibility(View.VISIBLE);
        timesRecyclerView.setVisibility(View.GONE);
    }

    public void pickTimes(View view)
    {
        DialogFragment dialogFragment = new TimePickerFragment(this);
        dialogFragment.show(getSupportFragmentManager(), "timePicker");
    }

    public void pickIntervals(final View view)
    {
        Log.d(LOG, "got in here");
        AlertDialog.Builder intervalDialogBuilder = new AlertDialog.Builder(this);
        intervalDialogBuilder.setTitle("Pick a reminder interval");
        // index                    0                        1                 2
        String [] choices = {"1 hour intervals", "Three hour intervals", "Six hour intervals"};
        intervalDialogBuilder.setItems(choices, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                switch(which)
                {
                    case 0:
                        Log.d(LOG, "1 hr interval pressed");
                        for(int i = 0; i < 24; i++)
                        {
                            addDailyTime(view.getContext(), i,0);
                        }
                        break;
                    case 1:
                        Log.d(LOG, "three hr interval pressed");
                        addDailyTime(view.getContext(), 0,0);
                        addDailyTime(view.getContext(), 3,0);
                        addDailyTime(view.getContext(), 6,0);
                        addDailyTime(view.getContext(), 9,0);
                        addDailyTime(view.getContext(), 12,0);
                        addDailyTime(view.getContext(), 15,0);
                        addDailyTime(view.getContext(), 18,0);
                        addDailyTime(view.getContext(), 21,0);
                        break;
                    case 2:
                        Log.d(LOG, "six hr interval pressed");
                        addDailyTime(view.getContext(), 0,0);
                        addDailyTime(view.getContext(), 6,0);
                        addDailyTime(view.getContext(), 12,0);
                        addDailyTime(view.getContext(), 18,0);
                        break;
                    default:
                }
            }
        });
        Dialog intervalDialog = intervalDialogBuilder.create();
        intervalDialog.show();
    }
    public void addDailyTime(Context con, int hourOfDay, int minute)
    {
        // TODO DO NOT ALLOW DUPLICATES TO BE ADDED
        // TODO ALSO THE TIMES LIST NEEDS TO BE SORTED AND REDISPLAYED AFTER A NEW TIME IS ADDED

        Log.d(LOG, "onTimeSet reached");

        noTimesSetTextview.setVisibility(View.GONE);
        timesRecyclerView.setVisibility(View.VISIBLE);

        DailyActivity dailyActivity = (DailyActivity)con;

        DailyTime dailyTime = new DailyTime(hourOfDay, minute, true, false, false);

        dailyActivity.dailyTimes.add(dailyTime);
        Log.d(LOG, "onTimeSet - Just added a new dailyTime, dailyTimes size is:" + dailyActivity.dailyTimes.size());

        //dailyTimeViewModel.insert(dailyTime);
        dailyActivity.numOfTimesCreated++;

        dailyActivity.dailyTimesListAdapter.notifyDataSetChanged();

        Log.d(LOG, "onTimeSet - dailyTimes count: " + dailyActivity.dailyTimes.size());
        Log.d(LOG, "onTimeSet - numOfTimesCreated: " + dailyActivity.numOfTimesCreated);

            /*if(dailyActivity.isInEditMode)
            {

            }
            else
            {

            }*/
    }
    public static class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener
    {
        private String LOG = "------> TimePickerFragment";
        private Context con;

        public TimePickerFragment(Context con)
        {
            this.con = con;
        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState)
        {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute)
        {
            ((DailyActivity)con).addDailyTime(con, hourOfDay, minute);
        }
    }
}

package com.example.dailies;

import android.content.Context;
import android.widget.Toast;

public final class Utility
{
    public static void Toast(Context con, String message)
    {
        Toast.makeText(con, message, Toast.LENGTH_LONG).show();
    }
    public enum Day
    {
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Sunday
    }
    public static Day convertFromInt(int x)
    {
        switch(x)
        {
            case 0:
                return Day.Monday;
            case 1:
                return Day.Tuesday;
            case 2:
                return Day.Wednesday;
            case 3:
                return Day.Thursday;
            case 4:
                return Day.Friday;
            case 5:
                return Day.Saturday;
            case 6:
                return Day.Sunday;
        }
        return null;
    }
}

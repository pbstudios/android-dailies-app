package com.example.dailies;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;

public class SettingsActivity extends AppCompatActivity
{
    private SharedPreferences sharedPreferences;
    public static String sharePrefFile = "com.example.dailies.sharedprefs";
    public static String DESC_SWITCH_TAG =  "showDescSwitch";

    private Switch showDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setttings);

        showDesc = findViewById(R.id.hide_desc_switch);
        sharedPreferences = getSharedPreferences(sharePrefFile, MODE_PRIVATE);

        showDesc.setChecked(sharedPreferences.getBoolean(DESC_SWITCH_TAG,true));

        showDesc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                SharedPreferences.Editor preferencesEditor = sharedPreferences.edit();
                preferencesEditor.putBoolean(DESC_SWITCH_TAG, isChecked);
                preferencesEditor.apply();
            }
        });
    }
}

package com.example.dailies;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

//@Entity(tableName="daily_time_table")
public class DailyTime implements Serializable
{
    private String LOG = "------> DailyTime";
    //@PrimaryKey(autoGenerate = true)
    //@NonNull
    //@ColumnInfo
    int ID;

    //@NonNull
    //@ColumnInfo(name="hour")
    private int hour;

    //@NonNull
    //@ColumnInfo(name="minute")
    private int minute;

    //@NonNull
    //@ColumnInfo(name="activated")
    private boolean isSwitchActivated;

    private boolean isCheckBoxVisible;
    private boolean isCheckBoxChecked;
    public DailyTime(int hour, int minute, boolean switchActivated, boolean checkBoxVisible, boolean checkBoxChecked)
    {
        this.hour = hour;
        this.minute = minute;

        this.isSwitchActivated = switchActivated;
        this.isCheckBoxVisible = checkBoxVisible;
        this.isCheckBoxChecked = checkBoxChecked;
    }
    public int getHour()
    {
        return hour;
    }
    public int getMinute()
    {
        return minute;
    }
    public int getID()
    {
        return ID;
    }


    @NonNull
    @Override
    public String toString()
    {
        Log.d(LOG, "BEFORE TRANSFORM -> " + hour + " , " + minute);

        String amPm = "AM";
        String minuteString = minute + "";

        int tempHour = 0;

        if(hour >= 12)
        {
            amPm = "PM";
            if (hour > 12)
            {
                tempHour = hour - 12;
            }
            else
            {
                tempHour = hour;
            }
        }
        else if(hour <= 12)
        {
            amPm = "AM";
            if(hour == 0)
            {
                tempHour = 12;
            }
            else
            {
                tempHour = hour;
            }
        }
        String hourString = tempHour + "";

        if(hourString.length() == 1)
        {
            hourString = "0" + hourString;
        }
        if(minuteString.length() == 1)
        {
            minuteString = "0"+ minuteString;
        }
        Log.d(LOG, "AFTER TRANSFORM -> " + hourString + " : " + minuteString + " " + amPm + "\n");

        return hourString + " : " + minuteString + " " + amPm;
    }
    public boolean isSwitchActivated()
    {
        return this.isSwitchActivated;
    }
    public void setSwitchActivated(boolean isChecked)
    {
        this.isSwitchActivated = isChecked;
    }
    public void setCheckBoxChecked(boolean isChecked)
    {
        this.isCheckBoxChecked = isChecked;
    }
    public boolean isCheckBoxChecked()
    {
        return this.isCheckBoxChecked;
    }
    public boolean isCheckBoxVisible()
    {
        return this.isCheckBoxVisible;
    }
    public void setCheckBoxVisible(boolean visible)
    {
        this.isCheckBoxVisible = visible;
    }
}
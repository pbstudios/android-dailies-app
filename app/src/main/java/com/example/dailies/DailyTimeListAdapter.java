package com.example.dailies;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class DailyTimeListAdapter extends RecyclerView.Adapter<DailyTimeListAdapter.DailyTimeViewHolder>
{
    private final LayoutInflater inflater;
    private List<DailyTime> dailyTimes;

    private Context con;
    private DailyTime dailyTime = null;

    public DailyTimeListAdapter(Context con)
    {
        this.con = con;
        inflater = LayoutInflater.from(con);
    }
    /* VIEW HOLDER START */
    public class DailyTimeViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        private String LOG = "------> DailyTimeViewHolder";
        private final CheckBox dailyTimeCheckbox;
        private final TextView dailyTimeTextView;
        private final Switch dailyTimeSwitch;

        public DailyTimeViewHolder(@NonNull View itemView)
        {
            super(itemView);
            itemView.setOnClickListener(this);

            dailyTimeCheckbox = itemView.findViewById(R.id.delete_checkbox);
            dailyTimeTextView = itemView.findViewById(R.id.time_list_item_time);
            dailyTimeSwitch = itemView.findViewById(R.id.time_list_item_switch);
        }

        @Override
        public void onClick(View v)
        {
            Log.d(LOG, "you tapped a single time");
        }
    }
    /* VIEW HOLDER END */

    //////////////////////////////////
    @NonNull
    @Override
    public DailyTimeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        View itemView = inflater.inflate(R.layout.time_list_item,parent, false);
        return new DailyTimeViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(@NonNull DailyTimeViewHolder holder, final int position)
    {
        if(dailyTimes != null)
        {
            dailyTime = dailyTimes.get(position);

            holder.dailyTimeCheckbox.setVisibility(determineIfCheckBoxIsVisible(dailyTime.isCheckBoxVisible()));
            holder.dailyTimeCheckbox.setChecked(dailyTime.isCheckBoxChecked());

            holder.dailyTimeTextView.setText(dailyTime.toString());
            holder.dailyTimeSwitch.setChecked(dailyTime.isSwitchActivated());

        }
        else
        {
            holder.dailyTimeCheckbox.setVisibility(View.GONE);
            holder.dailyTimeCheckbox.setChecked(false);

            holder.dailyTimeTextView.setText("empty time");
            holder.dailyTimeSwitch.setChecked(false);
        }
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View v)
            {
                ((DailyActivity)con).deleteTime();
                return true;
            }
        });
        holder.dailyTimeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                dailyTimes.get(position).setSwitchActivated(isChecked);
            }
        });
        holder.dailyTimeCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                dailyTimes.get(position).setCheckBoxChecked(isChecked);
            }
        });
    }
    public int determineIfCheckBoxIsVisible(boolean visible)
    {
        if(visible)
        {
            return View.VISIBLE;
        }
        else return View.GONE;
    }

    @Override
    public int getItemCount()
    {
        if(dailyTimes != null)
        {
            return dailyTimes.size();
        }
        else return 0;
    }
    public List<DailyTime> getDailyTimes()
    {
        return this.dailyTimes;
    }
    void setDailyTimes(List<DailyTime> dailyTimes)
    {
        this.dailyTimes = dailyTimes;
        notifyDataSetChanged();
    }
    public DailyTime getDailyAtPosition(int position)
    {
        return dailyTimes.get(position);
    }
}
